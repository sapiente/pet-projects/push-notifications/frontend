#!/bin/bash

openssl req \
    -newkey rsa:2048 -nodes -keyout localhost.key \
    -config localhost.cnf \
    -x509 -days 365 -out localhost.pem