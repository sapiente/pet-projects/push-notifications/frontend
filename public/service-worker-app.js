self.addEventListener('push', function(e) {
    const notification = JSON.parse(e.data.text());

    e.waitUntil(self.registration.showNotification(notification.title, {body: notification.body}));
});