import React, {useState, useEffect} from 'react';
import {isSubscribed, subscribeUser, unsubscribeUser} from "./pushNotifications";

function PushNotificationsButton() {
    const [subscribed, setSubscribed] = useState(false);

    useEffect(() => {
        isSubscribed().then(setSubscribed)
    }, []);

    return (
        <div className="fab" onClick={() => setSubscribed(!subscribed)}>
            <div className="fab__ripple"/>
            {subscribed ? (
                <img className="fab__image" src="/images/push-off.png" alt="Push Notification"
                     onClick={() => unsubscribeUser().then(() => setSubscribed(false))}/>
            ) : (
                <img className="fab__image" src="/images/push-on.png" alt="Push Notification"
                     onClick={() => subscribeUser().then(() => setSubscribed(true))}/>
            )}
        </div>
    );
}

export default PushNotificationsButton;